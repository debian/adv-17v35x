adv-17v35x (5.0.7.0-1) unstable; urgency=medium

  * QA upload.

  [ Andreas Beckmann ]
  * New upstream release.
  * Bump Standards-Version to 4.6.2, no changes needed.

  [ Paolo Pisati ]
  * Fix module build for Linux 6.1. (Closes: #1030187)

 -- Andreas Beckmann <anbe@debian.org>  Wed, 08 Feb 2023 17:11:13 +0100

adv-17v35x (5.0.6.0-4) unstable; urgency=medium

  * QA upload.
  * Fix build with linux 6.0 (Closes: #1023662)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 19 Nov 2022 08:37:41 +0100

adv-17v35x (5.0.6.0-3) unstable; urgency=medium

  * QA upload.
  * Switch to dh-sequence-dkms.
  * Declare Testsuite: autopkgtest-pkg-dkms.
  * Set Rules-Requires-Root: no.
  * Use pkg-info.mk.
  * Drop superfluous executable permission.
  * Drop Debian revision from dkms module version.
  * Do not generate dkms.conf.
  * Bump Standards-Version to 4.6.1 (no additional changes needed).

 -- Andreas Beckmann <anbe@debian.org>  Tue, 31 May 2022 22:21:36 +0200

adv-17v35x (5.0.6.0-2) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 25 May 2022 15:38:03 +0100

adv-17v35x (5.0.6.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Use canonical URL in Vcs-Git.

  [ Alexander Gerasiov ]
  * New upstream version 5.0.6.0
  * d/copyright: Update copyright entries.
  * Remove 0001-Update-for-timer-api-changes-in-Linux-4.15.patch. Fixed upstream.
  * d/control: Orphan the package (Closes: #979640).
  * d/control: Fix description (add new adapters).
  * d/control: Move package to salsa.

 -- Alexander GQ Gerasiov <gq@debian.org>  Sun, 31 Jan 2021 19:31:23 +0300

adv-17v35x (5.0.3.0-2) unstable; urgency=medium

  * Add 0001-Update-for-timer-api-changes-in-Linux-4.15.patch, thanks to
    Seth Forshee.
  * Bump Standards-Version to 4.1.3 (no additional changes needed).
  * debian/control: Set section to kernel according to lintian warning.

 -- Alexander GQ Gerasiov <gq@debian.org>  Sat, 10 Feb 2018 00:13:32 +0300

adv-17v35x (5.0.3.0-1) unstable; urgency=low

  * Initial release (closes: #872795).

 -- Andrey Drobyshev <immortalguardian@redlab-i.ru>  Mon, 14 Aug 2017 11:16:58 +0300
